suppressPackageStartupMessages({
  library(tidyverse)
})

theme_set(theme_classic())

args <- commandArgs(trailingOnly = TRUE)

# to test
# args <- c(
#   'data/pseudotime_analysis/Purkinje/all_genes_trajectory_summary.csv',
#   'data/pseudotime_analysis/Purkinje/pseudobulk_combined.rds',
#   'data/pseudotime_analysis/Purkinje/mfuzz_cluster_order.csv',
#   'tmp/tmpout'
# )

# data reading and prep --------------------------------------------------------

# folder to save the data in
outfolder <- args[length(args)]

# reading the main DF
df_all_data <- read_csv(args[1])

cluster.order <- read_csv(args[3]) %>% 
  mutate(cluster = as.factor(cluster)) %>% 
  mutate(cluster = fct_reorder(cluster, mean_com)) %>% 
  arrange(cluster)

df_all <- df_all_data %>% 
  mutate(gene_status = ifelse(!is.na(mf_class), 'hvg', 'bg')) %>% 
  filter((HUM_select_gene & 
            MOU_select_gene & 
            OPO_select_gene) |
           (HUM_select_gene == 0 &
              MOU_select_gene == 0 &
              OPO_select_gene == 0)) %>% 
  filter(!mf_class %in% c('Mouse specific',
                          'Opossum specific')) %>% 
  mutate(gene_status_detail = case_when(
    mf_class == 'conserved' ~ 'hvg - conserved',
    mf_class == 'diverged' | mf_class == 'Human specific' ~ 'hvg - diverged',
    mf_class != 'conserved' ~ 'hvg - intermediate',
    is.na(mf_class) & HUM_select_gene ~ 'hvg - not clustered',
    TRUE ~ gene_status
  )) %>% 
  mutate(gene_status_detail = factor(gene_status_detail,
                                     levels = c(
                                       'hvg - conserved',
                                       'hvg - intermediate',
                                       'hvg - diverged',
                                       'hvg - not clustered',
                                       'bg'
                                     ))) %>% 
  mutate(clusters_grouped = case_when(
    mf_cluster_HUM %in% cluster.order$cluster[1:2] ~ '01_early',
    mf_cluster_HUM %in% cluster.order$cluster[3:6] ~ '02_mid',
    mf_cluster_HUM %in% cluster.order$cluster[7:8] ~ '03_late'
  ))

df <- df_all %>% 
  drop_na()

pb <- readRDS(args[2])


p.enrich.cutoff <- .1
color.scale <- rev(viridis::inferno(100))[10:100]

gdf <- read_csv('data/metadata/all_genes_meta.csv')

# COLORS
species.colors <- c(
  Human = '#E59638',
  Mouse = '#000000',
  Opossum = '#50B3D1'
)

gene_status_detail.colors <- RColorBrewer::brewer.pal(5, 'BrBG')
names(gene_status_detail.colors) <- c('hvg - conserved',
                                      'hvg - intermediate',
                                      'bg',
                                      'hvg - diverged',
                                      'hvg - not clustered')

# normalized expression
nrm <- pb %>% 
  map(function(x) t(t(x) / colSums(x)) * 1e6)

nrm_scaled <- nrm %>% 
  map(function(x) t(scale(t(x[df$gene_name, ]))))

# FUNCTIONS --------------------------------------------------------------------

calc_enrich <- function(x, group, condition){
  x %>% 
    group_by({{group}}, {{condition}}) %>% 
    dplyr::count() %>% 
    group_by({{group}}) %>% 
    mutate(n_group = sum(n)) %>% 
    group_by({{condition}}) %>% 
    mutate(n_condition = sum(n)) %>% 
    ungroup() %>% 
    mutate(n_universe = sum(n),
           prop_group = n / n_group,
           prop_universe = n_condition / n_universe,
           emp_enrich = prop_group / prop_universe,
           pvalue = phyper(n-1, n_condition, n_universe - n_condition, n_group,
                           lower.tail = FALSE),
           p_adjust = p.adjust(pvalue, method = 'BH') + 1e-5) 
}

plot_enrich <- function(x, 
                        group,
                        condition,
                        color.scale = NULL,
                        p.enrich.cutoff){
  # x %>% 
  #   drop_na() %>% 
  #   ggplot(aes(x = 1-log10(p_adjust),
  #              y = {{condition}})) +
  #   scale_color_gradientn(colors = color.scale) +
  #   geom_vline(xintercept = 1-log10(p.enrich.cutoff),
  #              linetype = 'dashed') +
  #   geom_point(aes(size = n, 
  #                  color = emp_enrich)) +
  #   scale_size_continuous(range = c(1,5)) +
  #   facet_wrap(vars({{group}})) +
  #   theme(panel.spacing.x = unit(2.5, 'mm'),
  #         panel.grid.major.y = element_line(size = .5)) +
  #   labs(
  #     color = 'enrichment'
  #   )
  
  x %>% 
    drop_na() %>% 
    ggplot(aes(x = emp_enrich,
               y = {{condition}} )) +
    geom_vline(xintercept = 1,
               linetype = 'dashed') +
    geom_point(aes(size = n, 
                   color = p_adjust < p.enrich.cutoff)) +
    scale_size_continuous(range = c(1,5)) +
    facet_wrap(vars({{group}})) +
    theme(panel.spacing.x = unit(2.5, 'mm'),
          panel.grid.major.y = element_line(size = .5)) +
    labs(
      color = paste0('p-value < ', p.enrich.cutoff),
      x = 'enrichment'
    )
}

bootstrap_diff <- function(x,
                           group_col,
                           value_col,
                           n_reps = 10000){
  
  null.dist <- lapply(1:n_reps, function(i){
    set.seed(i)
    x %>%
      mutate(perm_value = sample({{value_col}},
                                 size = n(),
                                 replace = TRUE)) %>%
      group_by({{group_col}}) %>%
      summarise(mean_x = mean(perm_value)) %>%
      add_column(i = i)
  }) %>%
    do.call(rbind,.) %>%
    spread({{group_col}}, mean_x)
  
  observed.means <- x %>%
    group_by({{group_col}}) %>%
    summarise(observed_mean = mean({{value_col}})) %>%
    spread({{group_col}}, observed_mean)
  
  return(list(
    boostrapped = null.dist,
    observed_means = observed.means
  ))
}

# TODO mfuzz cluster distribution among the hvg classes ------------------------
# df_all_data
df_all %>% 
  filter(!is.na(mf_cluster_HUM)) %>% 
  mutate(mf_cluster_HUM = factor(mf_cluster_HUM,
                                 levels = cluster.order$cluster)) %>% 
  group_by(mf_cluster_HUM, 
           gene_status_detail) %>% 
  dplyr::count() %>% 
  group_by(mf_cluster_HUM) %>% 
  mutate(rel = n / sum(n) * 100) %>% 
  arrange(mf_cluster_HUM, 
          desc(gene_status_detail)) %>% 
  mutate(rel_cumsum = cumsum(rel) - (rel / 2)) %>% 
  ggplot(aes(x = mf_cluster_HUM,
             y = rel)) +
  geom_bar(aes(fill = gene_status_detail),
           stat = 'identity') +
  geom_text(
    mapping = aes(label = n,
                  y = rel_cumsum),
  ) +
  scale_fill_brewer(type = 'div') +
  labs(
    x = 'fuzzy cluster',
    y = 'proportion [%]',
    fill = 'gene class'
  )
ggsave(file.path(outfolder,
                 'variable_genes_mfClusters_proportion.pdf'),
       width = 7, height = 4)

# TODO trajectory change river plot --------------------------------------------

# Plotting of trajectories -----------------------------------------------------
pdf(file.path(outfolder, 'gene_trajectories.pdf'),
    width = 5, height = 4, useDingbats = FALSE)

for(g in c('diverged', 'Human specific', 'Mouse specific', 'Opossum specific', 'conserved')){
  
  tmp <- df %>% filter(mf_class == g)
  
  for(gene in tmp$gene_name){
    
    plt <- nrm_scaled %>% 
      map2(names(nrm_scaled), function(x, n){
        x[gene, ] %>% 
          reshape2::melt() %>% 
          rownames_to_column('pseudotime') %>% 
          as_tibble(.) %>% 
          add_column(species = n)
      }) %>% 
      do.call(rbind,.) %>% 
      mutate(species = dplyr::recode(species, 
                    HUM = 'Human',
                    MOU = 'Mouse',
                    OPO = 'Opossum')) %>% 
      mutate(species = factor(species, levels = c('Mouse', 'Human', 'Opossum'))) %>% 
      ggplot(aes(x = pseudotime, y=value, 
                 group = species,
                 color = species)) +
      geom_point() +
      geom_line() +
      scale_color_manual(values = species.colors) +
      theme(axis.text.x = element_blank()) +
      labs(
        x = 'pseudotime bin',
        y = 'scaled expression',
        title = paste0(g, ': ', gene)
      ); print(plt)
    
  }
}
dev.off()


# tfdf <- read_tsv('data/metadata/gene_lists/Mus_musculus_TF.txt') %>% 
#   dplyr::rename(gene_name = Symbol)
set.seed(1234); df %>% 
  filter(mf_class == 'conserved') %>% 
  select(gene_name, ends_with('tau'), ends_with('com'), gene_class, mf_cluster_HUM) %>% 
  gather('variable', 'value', -c(gene_name, gene_class, mf_cluster_HUM)) %>% 
  mutate(species = str_extract(variable, '^...')) %>% 
  mutate(measurement = str_extract(variable, "...$")) %>% 
  select(-variable) %>% 
  spread(measurement, value) %>% 
  group_by(mf_cluster_HUM) %>% 
  mutate(cluster_COM = mean(com)) %>% 
  ungroup() %>% 
  mutate(mf_cluster_HUM = as.factor(mf_cluster_HUM)) %>% 
  mutate(mf_cluster_HUM = fct_reorder(mf_cluster_HUM, cluster_COM)) %>% 
  mutate(species = recode(species,
                          HUM = 'Human',
                          MOU = 'Mouse',
                          OPO = 'Opossum')) %>% 
  mutate(species = factor(species, levels = c('Mouse', 'Human', 'Opossum'))) %>% 
  ggplot(aes(x = mf_cluster_HUM, 
             y = tau,
             color = mf_cluster_HUM)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(height = 0, width = .25,
              size = .25) +
  scale_color_brewer(type = 'qual', palette = 2) +
  facet_wrap(vars(species)) +
  theme(panel.spacing.x = unit(10, units = 'mm'),
        legend.position = 'none') +
  labs(
    x = 'fuzzy cluster',
    y = 'pseudotime specificity'
  )
ggsave(
  file.path(outfolder, 'mfuzz_cluster_tau_distribution.pdf'),
  width = 8, height = 3, useDingbats = FALSE
)

# gene metadata
gmd <- read_csv('data/metadata/all_genes_meta.csv') %>% 
  filter(ortho_type == 'ortholog_one2one')

# TF analysis ------------------------------------------------------------------
tfdf <- read_tsv('data/metadata/gene_lists/Mus_musculus_TF.txt') %>%
  dplyr::rename(gene_name = Symbol)


tf.conserved.individual <- tfdf %>% 
  left_join(df) %>% 
  drop_na() %>% 
  filter(mf_class == 'conserved') %>% 
  select(gene_name, ends_with('com')) %>% 
  gather('gr', 'com', ends_with('com')) 


tf.conserved <- tf.conserved.individual %>% 
  group_by(gene_name) %>% 
  summarise(com = median(com)) %>% 
  arrange(com)

tf.express <- nrm_scaled %>% 
  map2(names(nrm_scaled), function(x, n){
    x[tf.conserved$gene_name, ] %>% 
      reshape2::melt() %>% 
      rownames_to_column('pseudotime') %>% 
      as_tibble(.) %>% 
      add_column(species = n)
  }) %>% 
  do.call(rbind,.) %>% 
  dplyr::rename(gene_name = Var1) %>% 
  left_join(df %>% select(gene_name, cluster_HUM)) %>% 
  unite(gr, species, gene_name, remove = FALSE) %>% 
  mutate(species = recode(
    species,
    HUM = 'Human',
    MOU = 'Mouse',
    OPO = 'Opossum'
  )) %>% 
  mutate(species = factor(species, levels = c('Opossum', 'Human', 'Mouse'))) %>%
  left_join(tf.conserved) %>% 
  mutate(cluster_HUM = as.factor(cluster_HUM)) %>% 
  mutate(cluster_HUM = fct_reorder(cluster_HUM, com)) %>% 
  # mutate(gene_name = paste0(gene_name, ' (', round(com, 3),')')) %>% 
  left_join(cluster.order %>% dplyr::rename(cluster_HUM = cluster)) %>% 
  mutate(cluster_HUM = fct_reorder(cluster_HUM, mean_com)) %>% 
  mutate(gene_name = fct_reorder(gene_name, com))


tf.express %>% 
  mutate(species = factor(species, 
                          levels = c('Opossum',
                                     'Human',
                                     'Mouse'))) %>% 
  ggplot(aes(Var2, species, fill = value)) +
  geom_tile() +
  facet_grid(vars(cluster_HUM, gene_name),
             scales = 'free_y',
             labeller = labeller(cluster_HUM = function(x) return(rep('', length(x))),
                                 gene_name = label_value)) +
  scale_fill_gradientn(colors = viridis::inferno(100)) +
  labs(
    x = 'pseudotime bin',
    y = '',
    fill = 'scaled\nexpression'
  ) +
  theme(
    axis.text.x = element_blank(),
    axis.text.y = element_blank(),
    axis.ticks.y = element_line(size = 0),
    panel.spacing.y = unit(0.25, units = 'mm'),
    strip.text.y = element_text(angle = 0, hjust = 0),
    strip.background.y = element_blank()
  )

ggsave(file.path(outfolder, 'TF_conserved_programme.pdf'),
       width = 5, height = .175 * length(unique(tf.express$gene_name)), useDingbats = FALSE)




# variable genes intersect -----------------------------------------------------

plt <- df_all_data %>% 
  select(gene_name, ends_with('select_gene')) %>% 
  gather('gr', 'value', ends_with('select_gene')) %>% 
  mutate(species = str_extract(gr, '^...')) %>% 
  mutate(species = dplyr::recode(
    species,
    HUM = 'Human',
    MOU = 'Mouse',
    OPO = 'Opossum'
  )) %>% 
  select(-gr) %>% 
  group_by(gene_name) %>% 
  filter(value == 1 ) %>% 
  summarise(species = list(species)) %>% 
  ggplot(aes(x = species)) +
  geom_bar() +
  ggupset::scale_x_upset() +
  theme_classic()
ggsave(file.path(outfolder, 'variable_genes_species_overlap.pdf'),
       width = 4, height = 3, plot = plt)

# cancer genes -----------------------------------------------------------------
cancer.genes <- read_csv('data/metadata/gene_lists/cancer-genes.csv') %>% 
  dplyr::rename(gene_name = `Common name`) %>% 
  left_join(gmd %>% filter(species == 'HUM')) %>% 
  select(ortho_name, group) %>% 
  distinct() %>% 
  dplyr::rename(gene_name = ortho_name)


df %>% 
  left_join(cancer.genes) %>% 
  select(gene_name, mf_class, group) %>% 
  drop_na() %>% 
  write_csv(file.path(outfolder, 'cancer_associated_genes.csv'))

cancer.associated.test <- df_all %>% 
  left_join(cancer.genes) %>% 
  calc_enrich(gene_status_detail, group)

cancer.associated.test %>% 
  write_csv(file.path(outfolder, 'variable_genes_cancer_associated_genes.csv'))

cancer.associated.test %>% 
  plot_enrich(gene_status_detail, group, color.scale, p.enrich.cutoff)

ggsave(file.path(outfolder, 'variable_genes_cancer_association.pdf'),
       width = 7, height = 3)


# conserved genes cluster enrichments
cdg_cancer <- df_all %>% 
  left_join(cancer.genes) %>% 
  filter(mf_class == 'conserved') %>% 
  calc_enrich(mf_cluster_HUM, group)

cdg_cancer %>% 
  plot_enrich(mf_cluster_HUM, group, color.scale, p.enrich.cutoff)


# Aldinger disease analysis -------------------------------------------------------------
disease.genes <- read_csv('data/metadata/gene_lists/Aldinger_disease_genes.csv') %>% 
  gather('disease', gene_name) %>% 
  left_join(gmd %>% filter(species == 'HUM')) %>% 
  select(ortho_name, disease) %>% 
  distinct() %>% 
  dplyr::rename(gene_name = ortho_name) %>% 
  drop_na()


df %>% 
  left_join(disease.genes) %>% 
  select(gene_name, mf_class, disease) %>% 
  drop_na() %>% 
  arrange(disease) %>% 
  write_csv(file.path(outfolder, 'disease_associtated_genes.csv'))


disease.association.test <- df_all %>% 
  left_join(disease.genes) %>% 
  calc_enrich(gene_status_detail, disease)

disease.association.test %>% 
  plot_enrich(gene_status_detail, 
              disease,
              color.scale,
              p.enrich.cutoff)

ggsave(file.path(outfolder, 'variable_genes_disease_association.pdf'),
       width = 7, height = 3)


# Dickinson pLI percentile -----------------------------------------------------
# data from https://www.nature.com/articles/nature19356#MOESM300
dickinson.df <- readxl::read_xlsx('data/metadata/Dickinson_S8.xlsx',
                                  sheet = 'Supplementary table 8', na = '.') %>% 
  dplyr::rename(gene_name = Gene_symbol) 

# QC for the table
# checks whether genes are annotated more than once
stopifnot(nrow(dickinson.df %>% group_by(gene_name) %>% filter(n() > 1)) == 0)

# gene name translation
dickinson.df <- dickinson.df %>% 
  left_join(gdf %>% filter(species == 'HUM')) %>% 
  filter(ortho_type == 'ortholog_one2one') %>% 
  mutate(gene_name = ortho_name)

# recheck
stopifnot(nrow(dickinson.df %>% group_by(gene_name) %>% filter(n() > 1)) == 0)

# overall distribution of pLI_perc
df_all %>% 
  left_join(dickinson.df) %>% 
  ggplot(aes(gene_status_detail, 
             pLI_perc,
             fill = gene_status_detail)) +
  geom_violin() +
  geom_boxplot(width=.25) +
  scale_fill_manual(values = gene_status_detail.colors) +
  theme(axis.text.x = element_text(angle=45,
                                   hjust=1,
                                   vjust=1))
ggsave(file.path(outfolder, 'variable_genes_Dickinson_pLIPerc_distribution.pdf'),
       width = 7, height = 4)

df_all %>% 
  left_join(dickinson.df) %>% 
  mutate(mf_cluster_HUM = factor(mf_cluster_HUM,
                                 levels = cluster.order$cluster)) %>% 
  ggplot(aes(mf_cluster_HUM,
             pLI_perc)) +
  geom_boxplot(aes(fill = gene_status_detail),
               outlier.size = .5) +
  scale_fill_manual(values = gene_status_detail.colors)
ggsave(file.path(outfolder, 'variable_genes_Dickinson_pLIPerc_distribution_mfClusters.pdf'),
       width = 8, height = 4)

df_all %>% 
  left_join(dickinson.df) %>% 
  ggplot(aes(clusters_grouped,
             pLI_perc)) +
  geom_boxplot(aes(fill = gene_status_detail),
               outlier.size = .5) +
  scale_fill_manual(values = gene_status_detail.colors)
ggsave(file.path(outfolder, 'variable_genes_Dickinson_pLIPerc_distribution_mfGroupedClusters.pdf'),
       width = 8, height = 4)



# Bartha lethality call --------------------------------------------------------
# filter for .85

bartha.df <- read_csv('data/metadata/Bartha_lethality_calls.csv') %>% 
  dplyr::rename(gene_id = Human_ID) %>% 
  left_join(gdf %>% filter(species == 'HUM')) %>% 
  filter(ortho_type == 'ortholog_one2one') %>% 
  select(-gene_name) %>% 
  dplyr::rename(gene_name = ortho_name)

bartha.dist <- df_all %>% 
  left_join(bartha.df) %>% 
  select(gene_name, 
         gene_status_detail,
         clusters_grouped,
         mf_cluster_HUM,
         Bartha_in_vivo,
         Bartha_in_vitro,
         Bartha_mouse_KO) %>% 
  mutate(mf_cluster_HUM = factor(mf_cluster_HUM, 
                                 levels = cluster.order$cluster))


# distributions
pdf(file.path(outfolder, 'variable_genes_BarthaLethality_distribution.pdf'),
    width = 8, height = 4, useDingbats = FALSE)
  print(bartha.dist %>% 
    ggplot(aes(gene_status_detail, 
               Bartha_in_vivo,
               fill = gene_status_detail)) + 
    geom_violin() +
    geom_boxplot(outlier.shape = NA,
                 width = .25) +
    scale_fill_manual(values = gene_status_detail.colors) +
    labs(
      x = '',
      y = 'lethality [in vivo]'
    ))
  
  print(
    bartha.dist %>% 
      ggplot(aes(mf_cluster_HUM, Bartha_in_vivo)) +
      geom_boxplot(aes(fill = gene_status_detail)) +
      scale_fill_manual(values = gene_status_detail.colors)
  )
  
  print(
    bartha.dist %>% 
      ggplot(aes(clusters_grouped, Bartha_in_vivo)) +
      geom_boxplot(aes(fill = gene_status_detail)) +
      scale_fill_manual(values = gene_status_detail.colors)
  )
  
  
  print(bartha.dist %>% 
    ggplot(aes(gene_status_detail, Bartha_in_vitro,
               fill = gene_status_detail)) + 
    geom_violin() +
    geom_boxplot(outlier.shape = NA,
                 width = .25) +
    scale_fill_manual(values = gene_status_detail.colors) +
    labs(
      x = '',
      y = 'lethality [in vitro]'
    ))
  
  print(
    bartha.dist %>% 
      ggplot(aes(mf_cluster_HUM, Bartha_in_vitro)) +
      geom_boxplot(aes(fill = gene_status_detail)) +
      scale_fill_manual(values = gene_status_detail.colors)
  )
  
  print(
    bartha.dist %>% 
      ggplot(aes(clusters_grouped, Bartha_in_vitro)) +
      geom_boxplot(aes(fill = gene_status_detail)) +
      scale_fill_manual(values = gene_status_detail.colors)
  )
dev.off()

# Bartha in vivo ---------------------------------------------------------------
bartha.dist %>% 
  # filter(!is.na(Bartha_mouse_KO)) %>% 
  mutate(gene_status_detail = factor(gene_status_detail,
                                     levels = rev(levels(gene_status_detail)))) %>% 
  ggplot(aes(gene_status_detail, 
             fill = as.factor(Bartha_mouse_KO))) +
  geom_bar(position = 'fill') +
  coord_flip() +
  labs(
    x = '',
    fill = 'Lethal\nmouse\nKO'
  )
ggsave(file.path(outfolder, 'variable_genes_BarthaLethality_mouseKO.pdf'),
       width = 5, height = 4)


bartha.lethal.enrich <- bartha.df %>% 
  mutate(is_lethal = ifelse(Bartha_in_vivo > 85, 'lethal', 'viable')) %>% 
  right_join(df_all) %>% 
  calc_enrich(gene_status_detail, is_lethal)

bartha.lethal.enrich %>% 
  plot_enrich(gene_status_detail,
              is_lethal,
              color.scale, p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_genes_BarthaLethality_inVivo_enrichment.pdf'),
       width = 7, height = 4, useDingbats = FALSE)


# testing whether there is a significant difference between
# the groups

# bootstrapping the null distribution
bartha.null <- bartha.dist %>% 
  filter(!is.na(Bartha_in_vivo)) %>% 
  bootstrap_diff(gene_status_detail,
                 Bartha_in_vivo)

# testing: conserved vs the others
gene.groups <- colnames(bartha.null$boostrapped)[2:ncol(bartha.null$boostrapped)]
to.test <- gene.groups[1:(length(gene.groups)-1)]

bartha.tests <- lapply(to.test, function(x){
  against <- gene.groups[gene.groups != x]
  names(against) <- paste0(x, '/', against)
  
  lapply(against, function(y){
    tmp <- bartha.null$boostrapped %>% pull(x) /
      bartha.null$boostrapped %>% pull(y)
    
    tmp <- sum(tmp > (bartha.null$observed_means %>% pull(x) /
          bartha.null$observed_means %>% pull(y))) + 1
    tmp / nrow(bartha.null$boostrapped)
  }) %>% 
    do.call('c',.)
}) %>% do.call('c',.) %>% 
  p.adjust(method = 'BH')

tibble(
  test = names(bartha.tests),
  p_adjust = bartha.tests,
  alternative = 'greater'
) %>% 
  write_csv(file.path(outfolder, 'variable_genes_BarthaLethality_inVivo_distribution_Pval.csv'))

# Bartha in vitro --------------------------------------------------------------
bartha.lethal.enrich <- bartha.df %>% 
  mutate(is_lethal = ifelse(Bartha_in_vitro > 85, 'lethal', 'viable')) %>% 
  right_join(df_all) %>% 
  calc_enrich(gene_status_detail, is_lethal)

bartha.lethal.enrich %>% 
  plot_enrich(gene_status_detail,
              is_lethal,
              color.scale, p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_genes_BarthaLethality_inVitro_enrichment.pdf'),
       width = 7, height = 4, useDingbats = FALSE)


# testing whether there is a significant difference between
# the groups

# bootstrapping the null distribution
bartha.null <- bartha.dist %>% 
  filter(!is.na(Bartha_in_vitro)) %>% 
  bootstrap_diff(gene_status_detail,
                 Bartha_in_vitro)

# testing: conserved vs the others
gene.groups <- colnames(bartha.null$boostrapped)[2:ncol(bartha.null$boostrapped)]
to.test <- gene.groups[1:(length(gene.groups)-1)]

bartha.tests <- lapply(to.test, function(x){
  against <- gene.groups[gene.groups != x]
  names(against) <- paste0(x, '/', against)
  
  lapply(against, function(y){
    tmp <- bartha.null$boostrapped %>% pull(x) /
      bartha.null$boostrapped %>% pull(y)
    
    tmp <- sum(tmp > (bartha.null$observed_means %>% pull(x) /
                        bartha.null$observed_means %>% pull(y))) + 1
    tmp / nrow(bartha.null$boostrapped)
  }) %>% 
    do.call('c',.)
}) %>% do.call('c',.) %>% 
  p.adjust(method = 'BH')

tibble(
  test = names(bartha.tests),
  p_adjust = bartha.tests,
  alternative = 'greater'
) %>% 
  write_csv(file.path(outfolder, 'variable_genes_BarthaLethality_inVitro_distribution_Pval.csv'))

# Bartha lethality union -------------------------------------------------------
bartha.lethal.enrich <- bartha.df %>% 
  mutate(is_lethal = case_when(
    Bartha_in_vitro > 85 & Bartha_in_vivo > 85 ~ 'lethal',
    Bartha_in_vivo > 85 ~ 'lethal - in vivo only',
    Bartha_in_vitro > 85 ~ 'lethal - in vitro only',
    TRUE ~ 'viable'
  )) %>% 
  right_join(df_all) %>% 
  calc_enrich(gene_status_detail, is_lethal)

bartha.lethal.enrich %>% 
  plot_enrich(gene_status_detail,
              is_lethal,
              color.scale, p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_genes_BarthaLethality_combined_enrichment.pdf'),
       width = 7, height = 4, useDingbats = FALSE)

# # Bartha mfuzz clusters --------------------------------------------------------
# bartha.mfuzz <- bartha.dist %>% 
#   left_join(df_all) %>% 
#   select(gene_name, Bartha_in_vivo, Bartha_in_vitro, mf_cluster_HUM, gene_status_detail) %>% 
#   filter(gene_status_detail != 'bg') %>% 
#   mutate(mf_cluster_HUM = factor(mf_cluster_HUM, 
#                                  levels = cluster.order$cluster)) %>%
#   group_by(mf_cluster_HUM, gene_status_detail) 
# 
# bartha.mfuzz %>% 
#   # filter(n() > 10) %>% 
#   ggplot(aes(mf_cluster_HUM, 
#              Bartha_in_vivo,
#              fill = gene_status_detail)) +
#   geom_boxplot(outlier.size = .25) +
#   # geom_jitter(aes(color = gene_status_detail),
#   #             size = .1,
#   #             alpha = .5,
#   #             height = 0)
# ggsave(file.path(outfolder, 'variable_genes_BarthaLethality_mfClusters_distribution.pdf'),
#        width = 7, height = 4)
  

# split conserved genes base on cluster ----------------------------------------

# mutation resistance ----------------------------------------------------------


lethal.genes <- readxl::read_xlsx('data/metadata/gene_lists/mouse_lethals.xlsx',
                                  sheet = 'Lethal genes') %>% 
  rbind(
    readxl::read_xlsx('data/metadata/gene_lists/mouse_lethals.xlsx', 
                      sheet = 'Subviable genes'),
    readxl::read_xlsx('data/metadata/gene_lists/mouse_lethals.xlsx', 
                      sheet = 'Viable genes')
  ) %>% 
  select(-gene_name, -gene_id) %>% 
  dplyr::rename(gene_name = gene_symbol) %>% 
  mutate(viability = case_when(
    Primary_viability %in% c('Homozygous - Lethal',
                             'Homozygous - Subviable') ~ 'lethal/subviable',
    TRUE ~ 'viable'
  ))

df %>% 
  left_join(lethal.genes) %>% 
  # mutate(Primary_viability = ifelse(is.na(Primary_viability), 'viable', Primary_viability)) %>%
  filter(mf_class == 'conserved') %>% 
  group_by(viability, mf_cluster_HUM) %>% 
  summarise(n = n(),
            com = mean(MOU_gene_com)) %>% 
  ungroup() %>% 
  mutate(mf_cluster_HUM = fct_reorder(as.factor(mf_cluster_HUM), com)) %>% 
  group_by(mf_cluster_HUM) %>% 
  mutate(n_tot = sum(n)) %>% 
  mutate(prop = n/n_tot * 100) %>% 
  select(viability, mf_cluster_HUM, prop) %>% 
  spread(viability, prop, fill = 0) %>% 
  arrange(mf_cluster_HUM) %>% 
  write_csv(file.path(outfolder, 'mfuzz_mouse_mutation_resistance.csv'))



mouse.mut.resistance <- df_all %>% 
  left_join(lethal.genes) %>% 
  calc_enrich(gene_status_detail, viability) 
  
mouse.mut.resistance %>% 
  write_csv(file.path(outfolder, 'variable_genes_mouse_mutation_resistance.csv'))

mouse.mut.resistance %>% 
  plot_enrich(gene_status_detail, viability, color.scale = color.scale, p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_gene_mouse_mutation_resistance.pdf'),
       width = 7, height = 3)


# disease association ----------------------------------------------------------
disease.association.df <- read_csv('data/metadata/HighLevelConcepts_Splice_DM_2017_1_genes2concept.csv') %>% 
  gather('disease', 'gene_name') %>% 
  left_join(gdf %>% 
              filter(species == 'HUM') %>% 
              filter(ortho_type == 'ortholog_one2one')) %>% 
  drop_na() %>% 
  mutate(gene_name = ortho_name) %>% 
  filter(disease %in% c('Psychiatric',
                        'Nervous system',
                        'Developmental'))

ulms.test <- df_all %>% 
  left_join(disease.association.df) %>% 
  calc_enrich(gene_status_detail, disease) 

ulms.test %>% 
  write_csv(file.path(outfolder, 'variable_genes_ULMS_disease.csv'))


ulms.test %>% 
  plot_enrich(gene_status_detail,
              disease,
              color.scale,
              p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_gene_ULMS_disease.pdf'),
       width = 7, height = 4)


# generating different groups of genes
tst.groups <- disease.association.df  %>% 
  select(gene_name, disease) %>% 
  distinct() %>% 
  group_by(gene_name) %>%
  summarise(
    gr1 = case_when(any(disease == 'Developmental') ~ 'Developmental'),
    gr2 = case_when(any(disease == 'Nervous system') ~ 'Nervous system'),
    gr3 = case_when(any(disease == 'Psychiatric') ~ 'Psychiatric'),
    gr4 = case_when(sum(disease %in% c('Nervous system', 'Developmental')) == 2 ~ 'Nervous system & Developmental'),  
    gr5 = case_when(sum(disease %in% c('Psychiatric', 'Developmental')) == 2 ~ 'Psychiatric & Developmental'),  
    gr6 = case_when(any(disease %in% c('Psychiatric', 'Nervous system')) ~ 'Psychiatric | Nervous system'),  
    gr7 = case_when(sum(disease %in% c('Psychiatric', 'Nervous system')) == 1 & any(disease %in% 'Developmental') ~ 
                      '(Psychiatric | Nervous system) & Developmental')
  ) 
  
pdf(file.path(outfolder, 'variable_genes_DiseaseGroups.pdf'),
    width = 9, height = 4)
for(i in paste0('gr', 1:7)){
  out <- df_all %>% 
    left_join(tst.groups) %>% 
    calc_enrich(gene_status_detail,
                !!sym(i))
  
  print(out %>% 
    plot_enrich(gene_status_detail,
                !!sym(i),
                color.scale, p.enrich.cutoff))
}
dev.off()



# using Margaridas aggregated annotations
m.s2 <- readxl::read_xlsx('data/metadata/gene_lists/Margarida_S2.xlsx',
                          skip = 2, na = 'NA') %>%
  dplyr::rename(gene_id = `Human ID`) %>%
  left_join(gdf %>% filter(species == 'HUM')) %>%
  filter(ortho_type == 'ortholog_one2one') %>%
  mutate(gene_name = ortho_name) %>% 
  filter(gene_name %in% unique(disease.association.df$gene_name))

ulms.reduced.test <- df_all %>% 
  left_join(m.s2) %>% 
  calc_enrich(gene_status_detail, System)

ulms.reduced.test %>% 
  write_csv(file.path(outfolder, 'variable_genes_ULMS_MargaridaS2_disease.csv'))

ulms.reduced.test %>% 
  plot_enrich(gene_status_detail,
              System,
              color.scale,
              p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_genes_ULMS_MargaridaS2_disease.pdf'),
       width = 7, height = 4)
# stopifnot(max(table(m.s2$ortho_id)) == 1)
# 
# 
# dm.enrich <- df_all %>%
#   left_join(m.s2) %>% 
#   calc_enrich(gene_status_detail, Disease)
# 
# dm.enrich %>% 
#   write_csv(file.path(outfolder, 'variable_genes_human_essentials_hypgeomtest.csv'))
# 
# dm.enrich %>% 
#   plot_enrich(gene_status_detail, Disease, color.scale, p.enrich.cutoff)
# ggsave(file.path(outfolder, 'variable_genes_human_essentials_hypgeomtest.pdf'),
#        width = 7, height = 3)
#   




# disease system ---------------------------------------------------------------
df.disease.system <- df_all %>%
  left_join(m.s2) %>% 
  calc_enrich(gene_status_detail, System)


df.disease.system %>% 
  write_csv(file.path(outfolder, 'variable_genes_disease_system_enrichment.csv'))

df.disease.system %>% 
  plot_enrich(gene_status_detail, System, color.scale, p.enrich.cutoff)
ggsave(
  file.path(outfolder, 'variable_genes_disease_system_enrichment.pdf'),
  width = 8, height = 5
)


mfclass.disease.system <- df_all %>% 
  left_join(m.s2) %>% 
  calc_enrich(mf_class, System)

mfclass.disease.system %>% 
  write_csv(file.path(outfolder, 'variable_genes_disease_system_class_enrichment.csv'))

mfclass.disease.system %>%
  mutate(mf_class = factor(mf_class, 
                           levels = c(
                             'Human specific',
                             'Mouse specific',
                             'Opossum specific',
                             'conserved',
                             'diverged',
                             'not assigned'
                           ))) %>% 
  plot_enrich(mf_class, System, color.scale, p.enrich.cutoff)
ggsave(file.path(outfolder, 'variable_genes_disease_system_class_enrichment.pdf'),
       width = 8, height = 6, useDingbats = FALSE)

# df.disease.mfclass <- df_all %>%
#   left_join(m.s2) %>% 
#   filter(!is.na(mf_class)) %>% 
#   # filter(mf_class != 'not assigned') %>% 
#   group_by(mf_class, System) %>% 
#   dplyr::count() %>% 
#   group_by(System) %>% 
#   mutate(n_system_tot = sum(n)) %>% 
#   group_by(mf_class) %>% 
#   mutate(n_mf_class_tot = sum(n)) %>% 
#   ungroup() %>% 
#   mutate(n_tot = sum(n),
#          prop_universe = n_system_tot / n_tot,
#          prop_class = n / n_mf_class_tot,
#          emp_enrich = prop_class / prop_universe) %>% 
#   mutate(pval = 1-phyper(n, n_system_tot, n_tot - n_system_tot, n_mf_class_tot),
#          p_adjust = p.adjust(pval, method = 'BH')) %>% 
#   arrange(p_adjust) 
# 


# df.disease.mfclass %>% 
#   ggplot(aes(emp_enrich, 1-log10(p_adjust + 1e-4))) +
#   geom_point() +
#   facet_wrap(vars(mf_class)) +
#   geom_hline(yintercept = 1-log10(0.05 + 1e-4),
#              linetype = 'dashed') +
#   ggrepel::geom_label_repel(
#     aes(label = System),
#     data = function(x){
#       x %>% filter(p_adjust < .1)
#     },
#     min.segment.length = 0,
#     alpha = .5
#   ) +
#   labs(
#     x = 'empirical enrichment',
#     y = '1 - log10( p_adjust + 1e-4 )'
#   ) +
#   theme_classic()





# LOEF metric ------------------------------------------------------------------
# data from: https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/constraint/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz
plof.df <- read_tsv('data/metadata/gene_lists/gnomad.v2.1.1.lof_metrics.by_gene.txt') %>% 
  dplyr::rename(gene_name = gene) %>% 
  left_join(
    gdf %>% 
      filter(species == 'HUM')
  ) %>% 
  filter(ortho_type == 'ortholog_one2one') %>% 
  mutate(gene_name = ortho_name)

df_all %>% 
  left_join(plof.df)  %>%
  ggplot(aes(gene_status_detail, oe_lof_upper,
             fill = gene_status_detail)) +
  geom_violin() +
  geom_boxplot(outlier.shape = NA,
               width = .25) + 
  scale_fill_manual(values = gene_status_detail.colors) +
  labs(
    x = 'variable gene class'
  ) +
  theme_classic()
ggsave(file.path(outfolder, 'variable_genes_oeLOF_distribution.pdf'),
       width = 7, height = 3)

df_all %>% 
  left_join(plof.df) %>% 
  ggplot(aes(clusters_grouped,
             oe_lof_upper)) +
  geom_boxplot(aes(fill = gene_status_detail)) +
  scale_fill_manual(values = gene_status_detail.colors)
ggsave(file.path(outfolder, 'variable_genes_oeLOF_distribution_mfGroupedClusters.pdf'),
       width = 7, height = 3)