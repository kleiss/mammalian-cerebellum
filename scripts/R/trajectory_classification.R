suppressPackageStartupMessages({
  library(tidyverse)
  library(SingleCellExperiment)
  library(pbmcapply)
  # source('scripts/R/figures/helpers.R')
})
theme_set(theme_classic())

# commandline arguments
args <- commandArgs(trailingOnly = TRUE)

# for testing
# args <- c(
#   'data/sce/integrated/lineages/GC_integrated.rds',
#   'data/csv/integrated_lineage_pseudotime_GC.csv',
#   25,
#   8,
#   'tmp/tmpout'
# )

sce_file <- args[1]
pseudotime_file <- args[2]
ncores <- as.numeric(args[3])
mfuzz_nclusters <- as.numeric(args[4])
output_folder <- args[length(args)]


# sce <- readRDS('data/sce/integrated/lineages/GC_integrated.rds')
sce <- readRDS(sce_file)
# pseudotime_df <- read_csv('data/csv/integrated_lineage_pseudotime_GC.csv')
pseudotime_df <- read_csv(pseudotime_file)

min_cells_per_pseudobulk <- 50
n_bins <- 15
gene_poisson_vmr_cutoff <- 1


species.colors <- c(
  Human = '#E59638',
  Mouse = '#000000',
  Opossum = '#50B3D1'
)

class.colors <- species.colors
names(class.colors) <- paste(names(class.colors), 'specific', sep = ' ')
class.colors <- c(
  class.colors,
  c(
    conserved = '#96B09E',
    diverged = 'darkorchid3',
    'not assigned' = '#ECECEC'
  )
)

dir.create(output_folder, recursive = TRUE, showWarnings = FALSE)

## MAIN ------------------------------------------------------------------------

# saving all QC plots to a single file
pdf(file.path(output_folder, 'QC_plots.pdf'), width = 5, height = 5, useDingbats = FALSE)

sce <- sce[,pseudotime_df$cell_id]
sce$pseudotime <- pseudotime_df$dpt[
  match(colnames(sce), pseudotime_df$cell_id)
]

# cutting the pseudotime in n_bins
sce$pseudotime_bins <- cut(sce$pseudotime, n_bins)

scl <- tapply(1:ncol(sce),
              sce$species,
              function(i) sce[,i])

# Making pseudobulk ------------------------------------------------------------
make_pseudobulk <- function(x, gr){
  tapply(
    1:ncol(x),
    gr,
    function(i){
      rowSums(x[,i,drop=FALSE])
    }, simplify = FALSE
  ) %>% 
    do.call(cbind,.)
}

# summarising UMIs to pseudobulks per
# pseudotime bin
pb_bio <- scl %>% 
  map(function(s){
    
    # adding the chromium version and
    # the biological replicate ID to the
    # grouping vector
    s$gr <- paste(
      s$TissueID,
      s$`Capture System`,
      s$pseudotime_bins,
      sep = '//'
    )
    
    # removing pseudobulks with low number
    # of cells
    gr_table <- table(s$gr)
    keep_pb <- names(gr_table)[gr_table >= min_cells_per_pseudobulk]
    s <- s[,s$gr %in% keep_pb]
    
    # initial pseudobulk per biological replicate
    make_pseudobulk(assay(s, 'umi'),
                    s$gr)
  })

# merging pseudobulks at same pseudotime bin
pb <- pb_bio %>% 
  map(function(x){
    # using the bin as the new grouping variable
    gr <- colnames(x)
    gr <- str_split_fixed(gr, '//', 3)[,3]
    
    # calculating mean expression for each
    # pseudotime bin
    tapply(1:ncol(x),
           gr,
           function(i) rowMeans(x[,i,drop=FALSE]),
           simplify = FALSE) %>% 
      do.call(cbind,.)
  })

# making sure that only pseudotime bins
# that are covered in all three species are used from here on
use_pb <- pb %>% map(colnames) %>% purrr::reduce(intersect)
stopifnot(length(use_pb)>5)
pb <- pb %>% map(function(x) x[,use_pb])

nrm <- pb %>% 
  map(function(x) t(t(x) / colSums(x)) * 1e6)


# Gene statistics --------------------------------------------------------------
gene.vars <- pb %>% 
  map(function(x) t(t(x) / colSums(x))) %>% 
  map(rowVars)

gene.means <- pb %>% 
  map(function(x) t(t(x) / colSums(x))) %>% 
  map(rowMeans)

gene.pvmr <- pb %>% 
  map(function(x) mean(1/colSums(x)))


gene.com <- pb %>% 
  map(function(x){
    x <- t(t(x) / colSums(x) * 1e6)
    # x <- x - apply(x, 1, min)
    # x <- x + apply(x, 1, max)
    colSums(t(x) * 1:ncol(x)) / Matrix::rowSums(x)
  })

gene_time_tau <- nrm %>% 
  map(function(x){
    # x <- log1p(x)
    # rowMaxs(x) / rowSums(x)
    rowSums(1 - (x / matrixStats::rowMaxs(x))) / (ncol(x) - 1)
  })

ncells <- scl %>% 
  map(function(s){
    x <- assay(s, 'umi') > 0
    rowSums(x)
  })

gene.stats <- pmap(
  list(gene.means, 
       gene.vars,
       gene.pvmr,
       ncells,
       names(ncells),
       gene.com,
       gene_time_tau),
  function(m, v, pvmr, n, species, com, ttau){
    tibble(
      gene_name = names(m),
      gene_mean = m,
      gene_var = v,
      gene_com = com,
      gene_poisson_vmr = pvmr,
      gene_time_tau = ttau,
      n_cells = n,
      species = species
    )
  }
) %>% 
  do.call(rbind,.) %>% 
  mutate(gene_vmr = gene_var / gene_mean) %>% 
  mutate(select_gene = gene_vmr > gene_poisson_vmr_cutoff * gene_poisson_vmr)

# Distance determination -------------------------------------------------------

# using only dispersed genes for downstream analysis
gene.stats %>% 
  filter(select_gene) %>% 
  group_by(gene_name) %>% 
  filter(n() == 3) %>% 
  pull(gene_name) %>% unique() -> use_genes

nrm_scaled <- nrm %>% 
  map(function(x) t(scale(t(x[use_genes,]))))



# Function for earth mover's distance
# emd <- function(x, y) sum( abs( cumsum( x - y ) ) )
emd <- function(x, y) {
  dtw::dtw(x,y, distance.only = TRUE)$distance
}

emd_dist <- pbmclapply(use_genes, function(g){
  tibble(
    gene_name = g,
    hm = emd(nrm_scaled$HUM[g,], 
             nrm_scaled$MOU[g,]),
    ho = emd(nrm_scaled$HUM[g,], 
             nrm_scaled$OPO[g,]),
    mo = emd(nrm_scaled$OPO[g,], 
             nrm_scaled$MOU[g,]),
  )
}, mc.cores = ncores) %>% 
  do.call(rbind,.)

# emd_bootstrap <- pbmcapply::pbmclapply(1:1000, function(i){
#   tmp <- nrm_scaled %>% 
#     map(function(x){ x[,sample(colnames(x), 
#                                size = ncol(x),
#                                replace = FALSE)] })
#   
#   lapply(use_genes, function(g){
#     tibble(
#       gene_name = g,
#       chain = i,
#       hm = emd(tmp$HUM[g,], 
#                tmp$MOU[g,]),
#       ho = emd(tmp$HUM[g,], 
#                tmp$OPO[g,]),
#       mo = emd(tmp$OPO[g,], 
#                tmp$MOU[g,]),
#     )
#   }) %>% 
#     do.call(rbind,.)
# }, mc.cores = ncores)
# emd_bootstrap <- do.call(rbind, emd_bootstrap) 
# emd_bootstrap <- emd_bootstrap %>% 
#   group_by(chain) %>% 
#   summarise_if(is.numeric, mean)
# bg_dist_distribution <- emd_bootstrap %>% 
#   mutate(bg_dist = rowMedians(cbind(hm,ho,mo))) %>% 
#   pull(bg_dist)
# 
# 
# same_dist_cutoff <- quantile(bg_dist_distribution, .1)


use_dist <- emd_dist%>% 
  gather('comp', 'dst', hm:mo) %>% 
  group_by(gene_name) %>% 
  mutate(
    max_d = max(dst),
    min_d = min(dst),
    mid_d = sort(dst)[2],
    max_comp = comp[dst == max(dst)],
    min_comp = comp[dst == min(dst)]
  ) %>% 
  ungroup() %>% 
  spread(comp, dst) %>% 
  mutate(max_min_ratio = max_d / min_d) %>% 
  mutate(mag = log2(sqrt(max_d * min_d))) %>% 
  mutate(mag = mag / max(mag)) %>% 
  mutate(score = max_min_ratio * mag) %>% 
  ungroup()


# saving pseudobulk data
saveRDS(pb, file = file.path(output_folder, 'pseudobulk_combined.rds'))
saveRDS(pb, file = file.path(output_folder, 'pseudobulk_bioRep.rds'))

# Running mfuzz ----------------------------------------------------------------
library(Mfuzz)

## creating the mfuzz matrix
mfm <- map2(nrm_scaled, names(nrm_scaled), function(x,n){
  rownames(x) <- paste0(n, "_", rownames(x))
  return(x)
}) %>% 
  do.call(rbind,.)


eset <- ExpressionSet(mfm)
set.seed(1234)
mf <- mfuzz(
  eset = eset, 
  centers = mfuzz_nclusters,
  m = 1.25
)


mfuzz.plot(eset, mf, mfrow = c(2,3), new.window = FALSE); par(mfrow = c(1,1))

mfcluster.mem <- as_tibble(mf$membership)
colnames(mfcluster.mem) <- paste('mf_mem_', colnames(mfcluster.mem), sep = '')

mfdf <- tibble(
  species_gene_name = names(mf$cluster),
  mf_cluster = mf$cluster
) %>% 
  cbind(mfcluster.mem) %>% 
  as_tibble(.) %>% 
  separate(species_gene_name,
           into = c('species',
                    'gene_name'),
           sep = '_')

mf.pval <- mfdf %>% 
  gather('mem_cluster', 'membership',
         starts_with("mf_mem")) %>% 
  group_by(gene_name, species) %>% 
  filter(max(membership) > .5) %>% 
  group_by(gene_name) %>% 
  filter(length(unique(species)) == 3) %>% 
  select(-mf_cluster) %>% 
  spread(species, membership) %>% 
  summarise(mf_p_hm = sum(HUM * MOU),
            mf_p_ho = sum(HUM * OPO),
            mf_p_mo = sum(MOU * OPO),
            mf_mem_h = max(HUM),
            mf_mem_m = max(MOU),
            mf_mem_o = max(OPO)) %>% 
  gather('test', 'pval', starts_with('mf_p')) %>% 
  ungroup() %>% 
  mutate(pval = p.adjust(pval, method = 'BH')) %>% 
  spread(test, pval)


# classifying the orthologs
diff_pval <- .05  # FDR cutoff
same_pval <- .5   # similarity cutoff
mf.pval <- mf.pval %>% 
  left_join(mfdf %>% 
              select(gene_name, species, mf_cluster) %>% 
              spread(species, mf_cluster) %>% 
              dplyr::rename(cluster_HUM = HUM,
                            cluster_MOU = MOU,
                            cluster_OPO = OPO)) %>% 
  mutate(mf_class = case_when(
    mf_p_hm < diff_pval & mf_p_ho < diff_pval & mf_p_mo > same_pval & cluster_MOU == cluster_OPO ~ 'Human specific',
    mf_p_hm < diff_pval & mf_p_ho > same_pval & cluster_HUM == cluster_OPO & mf_p_mo < diff_pval ~ 'Mouse specific',
    mf_p_hm > same_pval & cluster_HUM == cluster_MOU & mf_p_ho < diff_pval & mf_p_mo < diff_pval ~ 'Opossum specific',
    mf_p_hm > same_pval & mf_p_ho > same_pval & mf_p_mo > same_pval &
      cluster_HUM == cluster_MOU & cluster_HUM == cluster_OPO ~ 'conserved',
    mf_p_hm < diff_pval & mf_p_ho < diff_pval & mf_p_mo < diff_pval ~ 'diverged',
    TRUE ~ 'not assigned'
  ))
dev.off()

tfdf <- read_tsv('data/metadata/gene_lists/Mus_musculus_TF.txt') %>% 
  dplyr::rename(gene_name = Symbol) %>% 
  select(gene_name) %>% 
  mutate(gene_class = 'TF') %>% 
  distinct()

gene.summary <- use_dist %>% 
  left_join(mf.pval) %>% 
  left_join(
    mfdf %>% 
      select(gene_name, species, mf_cluster) %>% 
      spread(species, mf_cluster) %>% 
      dplyr::rename(mf_cluster_HUM = HUM,
                    mf_cluster_MOU = MOU,
                    mf_cluster_OPO = OPO)
  ) %>% 
  right_join(
    gene.stats %>% 
      gather('key', 'value', -c(gene_name, species)) %>% 
      unite(temp, species, key) %>% 
      spread(temp, value)
  ) %>% 
  left_join(tfdf) %>% 
  mutate(gene_class = ifelse(is.na(gene_class), 'no TF', gene_class))

# SAVING SUMMARY ---------------------------------------------------------------
write_csv(gene.summary, file.path(output_folder, 'all_genes_trajectory_summary.csv'))

# center of mass per cluster
cluster.stats <- gene.summary %>% 
  drop_na() %>% 
  select(gene_name, ends_with('gene_com'), starts_with('mf_cluster')) %>% 
  gather('cluster_species', 'cluster', starts_with('mf_cluster')) %>% 
  gather('species_com', 'com', ends_with('gene_com')) %>% 
  arrange(gene_name) %>% 
  mutate(cluster_species = str_extract(cluster_species, '...$')) %>% 
  mutate(species_com = str_extract(species_com, '^...')) %>% 
  filter(cluster_species == species_com) %>% 
  group_by(cluster) %>% 
  mutate(median_com = mean(com)) %>% 
  ungroup() %>% 
  mutate(cluster = as.factor(cluster)) %>% 
  mutate(cluster = fct_reorder(cluster, median_com))

cluster.stats %>% 
  group_by(cluster) %>% 
  summarise(mean_com = unique(median_com)) %>% 
  write_csv(file.path(output_folder, 'mfuzz_cluster_order.csv'))

dodge <- position_dodge(width = 0.8)
cluster.stats %>% 
  dplyr::rename(species = cluster_species) %>% 
  mutate(species = dplyr::recode(species, HUM = 'Human', MOU = 'Mouse', OPO = 'Opossum')) %>% 
  mutate(species = factor(species, levels = c('Mouse', 'Human', 'Opossum'))) %>% 
  ggplot(aes(y = com, 
             x = cluster)) +
  geom_violin(aes(fill = species), 
              position = dodge) + 
  scale_fill_manual(values = species.colors) +
  # geom_boxplot(aes(fill = species_com),
  #              width = .2,
  #              alpha = 0,
  #              position = dodge) +
  # stat_summary(fun = 'mean',
  #              geom = 'point') +
  labs(
    x = 'trajectory clusters',
    y = 'center of mass',
    fill = 'species'
  )
ggsave(
  file.path(output_folder, 'mfuzz_center_of_mass_distribution.pdf'),
  width = 7, height = 4, useDingbats = FALSE
)

species.colors <- c(species.colors,
                    c('not conserved' = 'gray'))

tmpdf <- nrm_scaled %>% 
  map2(names(nrm_scaled), function(x, n){
    x %>% 
      reshape2::melt() %>% 
      as_tibble(.) %>% 
      add_column(species = n) %>% 
      dplyr::rename(gene_name = Var1)
  }) %>% 
  do.call(rbind,.) %>% 
  left_join(cluster.stats %>% dplyr::rename(species = species_com)) %>% 
  drop_na() %>% 
  arrange(species, gene_name) %>% 
  left_join(gene.summary %>% select(gene_name, mf_class) %>% distinct()) %>% 
  mutate(species = dplyr::recode(species, HUM = 'Human', MOU = 'Mouse', OPO = 'Opossum')) %>% 
  mutate(tmp = case_when(
    mf_class == 'conserved' ~ 'conserved',
    TRUE ~ 'not conserved'
  )) %>% 
  group_by(tmp) %>% 
  sample_n(n()) %>% 
  mutate(species = case_when(
    tmp == 'conserved' ~ species,
    tmp == 'not conserved' ~ 'not conserved'
  )) %>% 
  ungroup() %>% 
  mutate(tmp = as.factor(tmp)) %>% 
  mutate(species = factor(species,
                          levels = c('Mouse',
                                     'Human',
                                     'Opossum',
                                     'not conserved'))) %>% 
  unite("species_gene", gene_name, species, remove = FALSE) %>% 
  mutate(species_gene = as.factor(species_gene)) %>% 
  mutate(species_gene = fct_reorder(species_gene, -as.numeric(tmp)))

tmpdf %>% 
  # mutate(species_gene = factor(species_gene, levels = rev(levels(species_gene)))) %>% 
  ggplot(aes(x = Var2, 
             y = value,
             group = species_gene,
             color = species)) + 
  geom_line(alpha = .5) +
  scale_color_manual(values = species.colors) +
  facet_wrap(vars(cluster)) +
  guides(color = guide_legend(override.aes = list(alpha = 1, size = 2))) +
  theme(axis.text.x = element_blank()) +
  labs(
    x = 'binned pseudotime',
    y = 'scaled expression'
  )
ggsave(
  file.path(output_folder, 'mfuzz_trajectories_per_species.pdf'),
  width = 7, height = 7, useDingbats = FALSE
)


nrm_scaled %>% 
  map2(names(nrm_scaled), function(x, n){
    x %>% 
      reshape2::melt() %>% 
      as_tibble(.) %>% 
      add_column(species = n) %>% 
      dplyr::rename(gene_name = Var1)
  }) %>% 
  do.call(rbind,.) %>% 
  left_join(cluster.stats %>% dplyr::rename(species = species_com)) %>% 
  drop_na() %>% 
  arrange(species, gene_name) %>% 
  left_join(gene.summary %>% select(gene_name, mf_class) %>% distinct()) %>% 
  filter(mf_class == 'conserved') %>% 
  unite("species_gene", species, gene_name, remove = FALSE) %>% 
  mutate(species_gene = factor(species_gene, levels = sample(unique(species_gene), length(unique(species_gene)), replace = FALSE))) %>% 
  mutate(species = dplyr::recode(species, HUM = 'Human', MOU = 'Mouse', OPO = 'Opossum')) %>% 
  ggplot(aes(x = Var2, 
             y = value,
             group = species_gene,
             color = species)) + 
  geom_line(alpha = .5) +
  scale_color_manual(values = species.colors) +
  facet_wrap(vars(cluster)) +
  guides(color = guide_legend(override.aes = list(alpha = 1, size = 2))) +
  theme(axis.text.x = element_blank())
ggsave(
  file.path(output_folder, 'mfuzz_trajectories_per_species_conserved.pdf'),
  width = 7, height = 7, useDingbats = FALSE
)


cluster.stats %>% 
  dplyr::rename(species = species_com) %>% 
  left_join(gene.summary) %>% 
  mutate(mf_class = factor(mf_class, levels = names(class.colors))) %>% 
  arrange(desc(mf_class)) %>% 
  ggplot(aes(max_d, min_d)) +
  geom_point(aes(color = mf_class)) +
  scale_color_manual(values = class.colors) +
  labs(
    x = 'maximum dtw distance observed',
    y = 'minimum dtw distance observed',
    color = 'trajectory class'
  )
ggsave(file.path(output_folder, 'classified_genes_distance_scatter.pdf'),
       width = 7, height = 6, useDingbats = FALSE)

gene.summary %>% 
  drop_na() %>% 
  group_by(mf_class, gene_class) %>% 
  dplyr::count() %>% 
  spread(gene_class, n) %>% 
  mutate(n = `no TF` + TF) %>%
  ungroup() %>% 
  mutate(rel_TF = TF / n) %>% 
  mutate(observed_genes = sum(TF + `no TF`),
         observed_TFs = sum(TF),
         observed_TF_rel = observed_TFs / observed_genes) %>%
  rowwise() %>% 
  mutate(tf_enrich_pval = 1-phyper(TF, observed_TFs, observed_genes - observed_TFs, n)) %>% 
  ungroup() %>% 
  mutate(tf_enrich_padjust = p.adjust(tf_enrich_pval, method = 'BH')) %>% 
  write_csv(file.path(output_folder, 'mfuzz_class_tf_enrichment.csv'))

gene.summary %>% 
  drop_na() %>% 
  filter(mf_class == 'conserved') %>% 
  group_by(mf_cluster_HUM, mf_cluster_MOU, mf_cluster_OPO, gene_class) %>%
  dplyr::count() %>% 
  spread(gene_class, n, fill = 0) %>% 
  arrange(desc(TF)) %>% 
  # filter(mf_cluster_HUM == mf_cluster_MOU & mf_cluster_MOU == mf_cluster_OPO) %>% 
  ungroup() %>% 
  mutate(n_TF_tot = sum(TF),
         n_nonTF_tot = sum(`no TF`)) %>% 
  mutate(tf_enrich_pval = 1-phyper(TF, n_TF_tot, n_nonTF_tot, TF + `no TF`) ) %>% 
  mutate(tf_enrich_padj = p.adjust(tf_enrich_pval)) %>% 
  write_csv(file.path(output_folder, 'mfuzz_conserved_cluster_tf_enrichment.csv'))



godf <- read_csv('data/metadata/mou_go_91.txt') %>% 
  dplyr::rename(gene_name = `Gene name`)

gene.summary %>% 
  drop_na() %>% 
  select(gene_name, mf_class, starts_with('mf_cluster')) %>% 
  filter(mf_class == 'conserved') %>% 
  filter(mf_cluster_HUM == mf_cluster_MOU & mf_cluster_MOU == mf_cluster_OPO) %>% 
  mutate(mf_cluster = mf_cluster_HUM) %>% select(-mf_cluster_MOU, -mf_cluster_OPO, -mf_cluster_HUM) %>% 
  left_join(godf) %>% 
  drop_na() %>% 
  group_by(mf_cluster, `GO term accession`, `GO term name`) %>% 
  dplyr::count() %>% 
  group_by(`GO term accession`) %>% 
  mutate(n_per_GO = sum(n)) %>% 
  filter(n_per_GO > 2) %>%   # only using GO terms with at least 3 annotated genes
  group_by(mf_cluster) %>% 
  mutate(n_per_cluster = sum(n)) %>% 
  ungroup() %>% 
  mutate(n_total = sum(n)) %>% 
  mutate(enrich_pval = 1-phyper(n, n_per_GO, n_total - n_per_GO, n_per_cluster)) %>% 
  mutate(enrich_padjust = p.adjust(enrich_pval, method = 'BH')) %>% 
  filter(enrich_padjust < .05) %>% 
  group_by(mf_cluster) %>% 
  arrange(mf_cluster, enrich_padjust) %>% 
  mutate(emp_enrichment = n/n_per_cluster / (n_per_GO/n_total)) %>% 
  arrange(mf_cluster, desc(emp_enrichment)) %>% 
  write_csv(
    file.path(output_folder, 'GO_conserved_trajectories.csv')
  )