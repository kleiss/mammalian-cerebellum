suppressPackageStartupMessages({
  library(tidyverse)
})

files_to_process <- c(
  neuron = 'data/csv/pca_all_species_celltype_neuron_variance.csv',
  all = 'data/csv/pca_all_species_celltype_variance.csv'
)

for( n in names(files_to_process) ){
  f <- files_to_process[n]
  df <- read_csv(f)

  plt <- df %>%
    ggplot(aes(
      x = `Principle component`,
      y = `Percentage variance explained`
    )) +
    geom_bar(
      stat = 'identity'
    )
  ggsave(glue::glue('figures/final/celltype_{n}_pca_variance.pdf'),
         plot = plt,
         width = 5,
         height = 4)
}
