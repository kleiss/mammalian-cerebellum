# MNN correct for combined data sets
library(Matrix)
library(tidyverse)
source("https://gist.githubusercontent.com/klprint/1ab4468eb3c54abcf0422dec6223b8fc/raw/96624adfe2f611b7c6568806d54c8062420d172c/single_cell_functions.R")

args <- commandArgs(trailingOnly = T)

# args <- c("generated_data/liger_output_combined/mouOpo_liger_out.rds")

lobj <- readRDS(args[1])

liger.dims <- do.call(rbind, lobj@H)

batches <- as.factor(qsplit(rownames(liger.dims), 2))

mnn.out <- batchelor::fastMNN(liger.dims, batch = batches, pc.input = T)

saveRDS(mnn.out, args[2])