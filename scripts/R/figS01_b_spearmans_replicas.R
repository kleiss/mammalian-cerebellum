## Figure S01 b
## Spearman's correlation per replica

## Imports
options(max.print = 100)
library(tidyverse)
library(SingleCellExperiment)

args = commandArgs(trailingOnly = TRUE)

## For debugging:
# args = c(
#   "data/sce/updated_exonic//mou_exonic_sce.rds",  # input SingleCellExperiment file
#   "TissueID,Capture.System,Stage,batch",  # comma separated list of grouping columns
#   "data/csv/spearmans_replica_mou.csv"  # output csv file
# )

## Parsing the commandline arugments
sce_file <- args[1]
grouping_vec <- str_split(args[2], ",")[[1]]
csv_out <- args[3]
gene_metadata_file <- args[4]

gene_metadata <- read_csv(gene_metadata_file) %>% 
  filter(chromosome_name %in% as.character(1:22))


## FUNCTIONS
combine_cells <- function(scs, grouping_col = "grouping_col"){

  m <- assay(scs, "umi")
  g <- colData(scs)[,grouping_col]

  out <- tapply(
    1:ncol(scs),
    g,
    function(x){
      Matrix::rowSums(m[,x,drop=FALSE])
    }
  )

  out <- do.call(cbind, out)

  return(out)
}

normalize <- function(x){
  out <- t(x) / colSums(x) * 1e6
  out <- log1p(out)
  # out <- apply(out, 1, function(y) y - median(y))
  out <- t(out)
  return(out)
}

colVars_spm <- function( spm ) {
  stopifnot( is( spm, "dgCMatrix" ) )
  ans <- sapply( seq.int(spm@Dim[2]), function(j) {
    mean <- sum( spm@x[ (spm@p[j]+1):spm@p[j+1] ] ) / spm@Dim[1]
    sum( ( spm@x[ (spm@p[j]+1):spm@p[j+1] ] - mean )^2 ) +
      mean^2 * ( spm@Dim[1] - ( spm@p[j+1] - spm@p[j] ) ) } ) / ( spm@Dim[1] - 1 )
  names(ans) <- spm@Dimnames[[2]]
  ans
}
rowVars_spm <- function( spm ) {
  colVars_spm( t(spm) )
}


get.info.genes <- function( counts ) {
  library(Matrix)
  stopifnot(!is.null(dim(counts)))
  norm_counts <- t(t(counts) / Matrix::colSums(counts))

  gene_means <- rowMeans( norm_counts )
  gene_vars <- rowVars_spm( norm_counts )
  poisson_vmr <- mean( 1 / Matrix::colSums( counts ) )
  
  vmr <- gene_vars / gene_means

  # informative_genes <- names(which(
  #   vmr  >  1.25 * poisson_vmr ))
  
  informative_genes <- names(sort(vmr, decreasing = TRUE))[1:1500]

  return(
    informative_genes
  )

}


## MAIN
sce <- readRDS(sce_file)
sce <- sce[rownames(sce) %in% gene_metadata$ensembl_gene_id, ]

tmp_md <- as.data.frame(colData(sce))
## print(colnames(tmp_md))

stopifnot(all(grouping_vec %in% colnames(tmp_md)))

sce$grouping_col <- apply(
  tmp_md[,grouping_vec],
  1,
  paste0,
  collapse="//"
)



umi <- combine_cells(sce, grouping_col = "grouping_col")
umi <- umi[rowSums(umi) > 50, ]
nrm <- normalize(umi)

## GENE SELECTION --------------------------------------------------------------
info_genes <- get.info.genes(counts = assay(sce, "umi")[rownames(umi),])


get_prop_genes <- function(s){
    x <- assay(s, 'umi')
    gr <- s$grouping_col

    tapply(1:ncol(x), gr, function(i){
      tmp <- x[,i,drop=FALSE] > 0
      tmp <- rowSums(tmp)
      names(tmp)[tmp > (.1 * length(i))]
    }, simplify = FALSE) %>%
      purrr::reduce(union)
}

info_genes <- get_prop_genes(sce)
cat('NUMBER OF GENES USED\n')
cat(unique(sce$species),': ',length(info_genes), '\n')
cat('----------------------')
# print(length(info_genes))
nrm <- nrm[info_genes, ]
## -----------------------------------------------------------------------------





cor_mat <- cor(nrm, method = "spearman")

cor_mat %>%
  as.data.frame() %>%
  rownames_to_column("pb") %>%
  as_tibble() %>%
  write_csv(csv_out)
