sce.correlationHeatmap <- function(sce_a, 
                                  sce_b, 
                                  group_by = "cell_type",
                                  use_hvg = T,
                                  hvg_combination_function = union,
                                  min_cells = 50, 
                                  subsample_to = 500, mc.cores=1){
  stopifnot(length(intersect(rownames(sce_a), rownames(sce_b))) > 100)
  
  sce <- list(sce_a, sce_b)
  
  sce <- lapply(sce, function(x) x[,!is.na(colData(x)[group_by])])
  
  pb <- lapply(sce, function(x){
    # x$gr <- paste0(colData(x)[,group_by], "//", colData(x)[,balance_by])
    x$gr <- colData(x)[,group_by]
    tmp <- sce.pseudobulk(x, group.by = "gr", mc.cores = mc.cores, min_cells = min_cells, subsample.to = subsample_to)
    
    # gr <- str_split_fixed(colnames(tmp), "//", n = 2)[,1]
    tmp <- assay(x, "umi")
    # tapply(1:ncol(tmp), x$gr, function(x) rowSums(tmp[,x,drop=F])) %>% do.call(cbind,.)
    
    return(tmp)
  })
  
  use_genes <- intersect(rownames(sce_a), rownames(sce_b))
  
  if(use_hvg){
    hvg <- list(get.info.genes(assay(sce_a, "umi")),
                get.info.genes(assay(sce_b, "umi"))) %>% 
      purrr::reduce(hvg_combination_function)
    
    use_genes <- intersect(hvg, use_genes)
  }
  
  pb <- lapply(pb, function(x) x[use_genes, ])
  
  pb <- lapply(pb, function(x) t(t(x) / colSums(x)) * 1e6)
  
  set.seed(1234)
  pheatmap(
    cor(
      pb[[1]] - rowMeans(pb[[1]]),
      pb[[2]] - rowMeans(pb[[2]]),
      method = "spearman"
    ),
    clustering_distance_rows = "euclidean",
    clustering_distance_cols = "euclidean"
  )
  
}
               
               
               
ftt <- function(x){
  cell_means <- colMeans(x)
  cell_means <- cell_means / mean(cell_means)
  
  m <- t(t(x) / cell_means)
  Matrix(sqrt(m) + sqrt(m + 1) -1, sparse = T)
}
               
count_levels <- function(x){
    tbl <- table(x)
    out <- as.numeric(tbl)
    names(out) <- names(tbl)
    
    return(out)
}

sce.normed.pb <- function(sce, 
                          group.by = "cell_type", 
                          min_cells = 100,
                          subsample_to = NULL,
                          norm_fun = ftt){
  
  
  gr <- colData(sce)[,group.by]
  
  gr_n <- count_levels(gr)
  gr_n <- gr_n[gr_n > min_cells]
   
  sce <- sce[,colData(sce)[,group.by] %in% names(gr_n)]
  gr <- colData(sce)[,group.by]
  x <- assay(sce, "umi")
  
  n <- tapply(1:ncol(x), gr, function(i) {
    tmp <- x[,i,drop=F]
    
    if(!is.null(subsample_to)){
        if(subsample_to < ncol(tmp)){
             tmp <- tmp[,sample(colnames(tmp), subsample_to), drop=F]
        }
    }
    
    rowSums(tmp)
  }) %>% do.call(cbind,.)
  
  # n <- t(t(n) / colSums(n) * 1e6)
  n <- norm_fun(n)
  
  return(n)
  
}
               
custom_cor <- function(x, y, method="spearman", hvg = intersect(rownames(x), rownames(y))){
    cor(
      as.matrix(x - rowMeans(x))[hvg, ],
      as.matrix(y - rowMeans(y))[hvg, ],
      method=method
    )
}
               
               
plot_corplot <- function(x, y, x_color, y_color, row_title = "x", column_title = "y", hvg = intersect(rownames(x), rownames(y)),
                         remove_pre = T,
                         method = "spearman"){
  #mat <- cor(
  #    as.matrix(x - rowMeans(x))[hvg, ],
  #    as.matrix(y - rowMeans(y))[hvg, ],
  #    method="spearman"
  #  )
  mat <- custom_cor(x, y, method=method, hvg = hvg)
  
  if(remove_pre){
    rownames(mat) <- str_remove(rownames(mat), "^.*_")
    colnames(mat) <- str_remove(colnames(mat), "^.*_")
  }
  
  
  row_list <- x_color[rownames(mat)]
  col_list <- y_color[colnames(mat)]
  
  #print(row_list)
  
  ha <- HeatmapAnnotation(
    Pseudobulk = names(col_list),
    col = list(Pseudobulk = col_list),
    show_legend = F
  )
  
  hb <- HeatmapAnnotation(
    Pseudobulk = names(row_list),
    col = list(Pseudobulk = row_list), 
    which = "row", show_legend = F
  )
  
  Heatmap(mat, name="Spearman", top_annotation = ha, left_annotation = hb, cluster_rows = F, 
          cluster_columns = F, row_title = row_title, column_title = column_title)
}
               
get.nn <- function(x, y=x, k=25, metric="cosine"){
  pd <- reticulate::import("pynndescent")

  cat("Building index\n")
  idx <- pd$NNDescent(x, metric = metric)
  
  cat("Finding nearest neighbors\n")
  nn <- idx$query(y, k=as.integer(k))
  
  names(nn) <- c("nn.idx", "nn.dist")
  
  return(nn)
}



sce.get.pseudostage <- function(x, y = x, x.stages, embedding="liger", k=25, metric="cosine"){
  nn <- get.nn(reducedDim(x, embedding), 
               reducedDim(y, embedding),
               k=k,
               metric=metric)$nn.idx
  
  out <- apply(nn, 1, function(i) mean(x.stages[i]))
  
  names(out) <- colnames(y)
  
  return(out)
}