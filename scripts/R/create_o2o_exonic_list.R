source("scripts/R/sce_helper.R")
library(tidyverse)

scs <- readRDS("data/sce/full_orthologs_sce.rds")
## scs <- lapply(scs,
##                      function(x){
##                        x[,!str_detect(x$Tissue, ' dn$')]
##                      })

gns_df <- read_csv("data/metadata/all_genes_meta.csv")


ex_dat <- lapply(c("hum", "mou", "opo"), function(sp){
  files <- list.files(glue::glue("../cerebellum/data/{sp}/raw/"), pattern=".h5", full.names = T)
  tmp <- lapply(files, read.h5, slot="exon")
  tmp %>% do.call(merge.sparse, .)
})

names(ex_dat) <- c("HUM", "MOU", "OPO")

scs_exonic <- lapply(names(scs), function(sp){
  
  ft <- scs[[sp]]
  ex <- ex_dat[[sp]]
  
  use_cells <- intersect(colnames(ft), colnames(ex))
  
  ft <- ft[,use_cells]
  ex <- ex[,use_cells]
  
  ex_genes <- gns_df %>% 
    filter(gene_id %in% rownames(ex)) %>% 
    filter(ortho_type == "ortholog_one2one")
  
  ex <- ex[ex_genes$gene_id, ]
  
  rownames(ex) <-ex_genes$ortho_id[match(rownames(ex), ex_genes$gene_id)]
  
  ex_sce <- SingleCellExperiment(assay = list(umi = ex), 
                                 colData = colData(ft),
                                 reducedDims = reducedDims(ft))
  
  return(ex_sce)
  
})

names(scs_exonic) <- names(scs)

shared_genes <- scs_exonic %>% map(rownames) %>% purrr::reduce(intersect)

scs_exonic <- scs_exonic %>% map(function(x) x[shared_genes, ])

saveRDS(scs_exonic, "data/sce/full_orthologs_sce_exonic.rds")
