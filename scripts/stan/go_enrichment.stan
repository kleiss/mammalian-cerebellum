
data {

}

parameters {

}

model {

}


// Binomial model with beta(1,1) prior
/* data { */
/*   int<lower=0> n;              // number of groups */
/*   int<lower=0> g;                // number of GO terms */
/*   int<lower=0> i[n];              // group index */
/*   int<lower=0> j[g];              // GO index */
/*   int<lower=0> N[n,g];              // number of experiments */
/*   int<lower=0> y[n,g];              // number of successes */
/* } */
/* parameters { */
/*   real<lower=0,upper=1> theta[n,g]; // probability of success in range (0,1) */
/* } */
/* model { */
/*   // model block creates the log density to be sampled */
/*   /\* theta[i,j] ~ beta(1, 1);          // prior *\/ */
/*   for (k in 1:g) { */

/*       y[i,k] ~ binomial(N[i,k], theta[i,k]);      // observation model / likelihood */

/*   } */
/*   // the notation using ~ is syntactic sugar for */
/*   //  target += beta_lpdf(theta | 1, 1);     // lpdf for continuous theta */
/*   //  target += binomial_lpmf(y | N, theta); // lpmf for discrete y */
/*   // target is the log density to be sampled */
/* } */
